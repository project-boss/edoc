﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Edocweb.Models {
    public class ApplicationUser : IdentityUser {
        public long EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

    }
}